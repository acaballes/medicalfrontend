<!doctype html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset("bootstrap/css/bootstrap.min.css") }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="{{ URL::asset("javascripts/physical_exam.js") }}"></script>
        <script src="{{ URL::asset("bootstrap/js/bootstrap.min.js") }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset("css/form.css") }}">
    </head>
    <title> Physical Exam </title>
<body>

<div class="container">
    <div class="col-md-4 c-info">
        <h2 class="sub-heading">Office Location</h2>
        <p><i class="glyphicon glyphicon-home"></i>RB Corp. and AC Logic</p>
        <p><i class="glyphicon glyphicon-map-marker"></i>Tagbilaran, Bohol, 6300</p>
        <p><i class="glyphicon glyphicon-phone"></i> +00 0123456789</p>
        <p><i class="glyphicon glyphicon-envelope"></i> startup_project@gmail.com</p>
        <p></p>
        <p></p>
    </div>


    <div class="col-md-8">
        <h2 class="sub-heading">Physical Examination Form</h2>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#patient_section">Patient Information</a></li>
            <li><a data-toggle="tab" href="#diagnosis_section">Diagnosis</a></li>
            <li><a data-toggle="tab" href="#laboratory_section">Laboratory Request</a></li>
            <!--<li><a data-toggle="tab" href="#prognosis_section">Prognosis</a></li>-->
        </ul>

        <form id="frm-physical-exam">
            <div class="tab-content">
                <div id="patient_section" class="tab-pane tab-section fade in active">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-search"></i></span>
                        <input name="search" type="text" list="patient_list" class="form-control" placeholder="Search Patient" required>
                    </div>

                    <datalist id="patient_list">
                        <option value="Algie Caballes">
                        <option value="Nino Sagisabal">
                        <option value="RE Guillen">
                        <option value="Ryan Baldisco">
                    </datalist>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-heart-empty"></i></span>
                        <input name="bp" type="text" class="form-control" placeholder="Blood Pressure - 120/80 mmHg" required>
                    </div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-heart"></i></span>
                        <input name="hr" type="text" class="form-control" placeholder="Heart Rate - 100 beats/min" required>
                    </div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-fire"></i></span>
                        <input name="temp" type="text" class="form-control" placeholder="Temperature - 37 degrees celsius" required>
                    </div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-scale"></i></span>
                        <input name="weight" type="text" class="form-control" placeholder="Weight - In kilograms" required>
                    </div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-sort"></i></span>
                        <input name="height" type="text" class="form-control" placeholder="Height - In centimeters" required>
                    </div>

                </div>

                <div id="diagnosis_section" class="tab-pane tab-section fade">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-pencil"></i></span>
                        <textarea name="diagnose" class="form-control" placeholder="Your diagnosis here" rows="5" required></textarea>
                    </div>
                </div>

                <div id="laboratory_section" class="tab-pane tab-section fade">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><input class="lab-request" type="checkbox" name="xray" id="xray" ></span>
                        <input type="text" class="form-control" placeholder="X-ray" disabled>
                    </div>
                    <div class="margin-bottom-10"></div>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><input class="lab-request" type="checkbox" name="urinalysis" id="urinalysis" ></span>
                        <input type="text" class="form-control" placeholder="Urinalysis" disabled>
                    </div>
                    <div class="margin-bottom-10"></div>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><input class="lab-request" type="checkbox" name="audiogram" id="audiogram" ></span>
                        <input type="text" class="form-control" placeholder="Audiogram" disabled>
                    </div>
                    <div class="margin-bottom-10"></div>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><input class="lab-request" type="checkbox" name="ctscan" id="ctscan" ></span>
                        <input type="text" class="form-control" placeholder="CT Scan" disabled>
                    </div>
                </div>

                <div id="prognosis_section" class="tab-pane tab-section fade">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-pencil"></i></span>
                        <textarea name="prognosis" class="form-control" placeholder="Your prognosis here" rows="5" required></textarea>
                    </div>
                </div>

                <div class="input-group input-group-lg">
                    <button type="submit" class="btn btn-primary"> Save </button>
                    <button type="reset" class="btn btn-primary"> Cancel </button>
                </div>
            </div>
        </form>
    <div class="alert alert-success" style="display: none;">
        <span class="glyphicon glyphicon-send"></span> &nbsp; Success! Data successfully save.
    </div>
    <div class="alert alert-danger" role="alert" style="display: none;">
        <span class="glyphicon glyphicon-exclamation-sign"></span> &nbsp; Please check the inputs.
    </div>
    </div>
</div>

</body>
</html>
