<!doctype html>
<html>
    <head>
        <title> @yield('title') </title>
        @include('style_script')
        @section('head')
            {{-- additional custom styling and scripts here --}}
        @show
    </head>

<body>
    @section('navigation')
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">MIS ver.1.0</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Patient Entry <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/patient_entry">Patient Registration</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Patient Queue</a></li>
                        <li><a href="#">Patient Scheduling</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Patient History</a></li>
                    </ul>
                </li>
                <li><a href="/physical_exam">Physical Examination</a></li>
                <li><a href="#contact">Contact</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
    @show

    <div class="container">
        @yield('content')
    </div>
</body>

</html>
