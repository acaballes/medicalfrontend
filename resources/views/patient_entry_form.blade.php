<!doctype html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="{{ URL::asset("javascripts/patient.js") }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset("bootstrap/css/bootstrap.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset("css/form.css") }}">
    </head>
    <title> Patient Entry </title>
<body>

<div class="container">
    <div class="col-md-4 c-info">
        <h2 class="sub-heading">Office Location</h2>
        <p><i class="glyphicon glyphicon-home"></i>RB Corp. and AC Logic</p>
        <p><i class="glyphicon glyphicon-map-marker"></i>Tagbilaran, Bohol, 6300</p>
        <p><i class="glyphicon glyphicon-phone"></i> +00 0123456789</p>
        <p><i class="glyphicon glyphicon-envelope"></i> startup_project@gmail.com</p>
        <p></p>
        <p></p>
    </div>
  
    <div class="col-md-8">
        <h2 class="sub-heading">Patient Entry Form</h2>
        <form id="frm-patient">
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user"></i></span>
                <input name="firstname" type="text" class="form-control" placeholder="Firstname" required>
            </div>

            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user"></i></span>
                <input name="lastname" type="text" class="form-control" placeholder="Lastname" required>
            </div>
            
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user"></i></span>
                <input name="gender" type="text" class="form-control" placeholder="Male/Female" required>
            </div>

            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
                <input name="email" type="email" class="form-control" placeholder="Email">
            </div>

            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-earphone"></i></span>
                <input name="contact_no" type="tel" class="form-control" placeholder="Phone Number" required>
            </div>

            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
                <input name="birthdate" type="text" class="form-control" placeholder="Month Day, Year" required>
            </div>

            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-pencil"></i></span>
                <textarea name="address" class="form-control" placeholder="Address" rows="5" required></textarea>
            </div>

            <div class="input-group input-group-lg">
                <button type="submit" class="btn btn-primary"> Save </button>
                <button type="reset" class="btn btn-primary"> Cancel </button>
            </div>
        </form>
  
        <div class="alert alert-success" style="display: none;">
            <span class="glyphicon glyphicon-send"></span> &nbsp; Success! Data successfully save.
        </div>
        <div class="alert alert-danger" role="alert" style="display: none;">
            <span class="glyphicon glyphicon-exclamation-sign"></span> &nbsp; Please check the inputs.
        </div>
    </div>

</div>
