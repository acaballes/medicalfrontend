@extends('master_layout')

@section('title', 'Medical Information System ver1.0')

@section('head')
    <script type="text/javascript" charset="utf-8" src="{{ URL::asset("javascripts/patient_history.js") }}"></script>
@stop

@section('content')
    @include('patient_history_table')
@stop
