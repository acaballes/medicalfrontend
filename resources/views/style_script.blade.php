<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::asset("bootstrap/css/bootstrap.min.css") }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset("datatable/DataTables-1.10.12/media/css/jquery.dataTables.min.css") }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset("css/master_layout.css") }}">
<script type="text/javascript" charset="utf-8" src="{{ URL::asset("bootstrap/js/bootstrap.min.js") }}"></script>
<script type="text/javascript" charset="utf-8" src="{{ URL::asset("datatable/DataTables-1.10.12/media/js/jquery.dataTables.min.js") }}"></script>
