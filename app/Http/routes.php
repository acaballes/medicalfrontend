<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/patient_history', function () {
    return view('patient_history');
});

Route::get('/get_patients_list', function () {
    return view('patients_list');
});

Route::get('/patient_entry/', function () {
    return view('patient_entry_form');
});

Route::get('/physical_exam/', function () {
    return view('physical_exam_form');
});

Route::get('action', 'Action@index');
Route::get('getpatient', 'Action@getPatient');
Route::get('savepatient', 'Action@savePatient');
Route::get('getdiagnosis', 'Action@getDiagnosis');
Route::get('savediagnosis', 'Action@saveDiagnosis');
Route::get('gethistory', 'Action@getHistory');
Route::get('savehistory', 'Action@saveHistory');
Route::get('getlogin', 'Action@getLogin');
Route::get('savelogin', 'Action@saveLogin');
Route::get('getmedicalexam', 'Action@getMedicalExam');
Route::get('savemedicalexam', 'Action@saveMedicalExam');
Route::get('getphysicalexam', 'Action@getPhysicalExam');
Route::get('savephysicalexam', 'Action@savePhysicalExam');
Route::get('getprognosis', 'Action@getPrognosis');
Route::get('saveprognosis', 'Action@savePrognosis');
Route::get('getremarks', 'Action@getRemarks');
Route::get('saveremarks', 'Action@saveRemarks');

Route::post('savepatient', 'Action@savePatient');
Route::post('savephysicalexam', 'Action@savePhysicalExam');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
