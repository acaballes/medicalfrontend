$(document).ready(function() {

    $('#frm-patient').submit(function(event) {
        var data = $(this).serializeArray();
        $.ajax({
            url: 'savepatient',
            type: 'POST',
            data: data,
            success: function(response) {
                if(response) {
                    $('.alert-success').fadeIn('slow', function() {
                        $('.alert-success').fadeOut(1200)
                    });
                }
            }
        });
        event.preventDefault();
    });

});

