$(document).ready(function () {

    $('#table').DataTable({
        ajax: {
            'url' : 'getpatient',
            'dataSrc' : 'result',
        },
        columns : [
            { 'data' : 'fname' },
            { 'data' : 'lname' },
            { 'data' : 'address' },
            { 'data' : 'phone' },
        ],
    });

});
